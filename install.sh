sudo apt-get install git vim tmux python3 python meld

# vim
mkdir -p ~/.vim/autoload ~/.vim/bundle && curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim


git clone https://github.com/tpope/vim-fugitive.git ~/.vim/bundle/fugitive
git clone https://github.com/scrooloose/nerdtree.git ~/.vim/bundle/nerdtree
git clone https://github.com/jistr/vim-nerdtree-tabs.git ~/.vim/bundle/vim-nerdtree-tabs
git clone https://github.com/vim-airline/vim-airline.git ~/.vim/bundle/vim-airline
git clone https://github.com/vim-airline/vim-airline-themes.git ~/.vim/bundle/vim-airline-themes


sudo apt-get install fonts-powerline
mkdir -p ~/tmp/_fonts_for_powerline && git clone https://github.com/powerline/fonts.git ~/tmp/_fonts_for_powerline && cp ~/tmp/_fonts_for_powerline/DejaVuSansMono/DejaVu\ Sans\ Mono\ for\ Powerline.ttf ~/.local/share/fonts/ && fc-cache -f -v
rm -rf ~/tmp/_fonts_for_powerline


# dconf read
# /org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/
# gnome-terminal preferences

# not system colors
# dark tango
# solarized
# not bold



# bashrc
mkdir -p ~/.bash-git-prompt && git clone https://github.com/magicmonty/bash-git-prompt.git ~/.bash-git-prompt
sed -i 's/#force_color_prompt/force_color_prompt/g' ~/.bashrc
sed -i 's/color_prompt=$/color_prompt=yes/g' ~/.bashrc
sed -i "$(sed -n '/xterm-color/=' .bashrc) a     xterm) color_prompt=yes;;" ~/.bashrc
sed -i 's/^xterm/    xterm/' ~/.bashrc 
sed -i 's/^unset color/#unset color/' ~/.bashrc 
sed -i 's/PS1=\"\\\[\\e\]/# PS1=\"\\\[\\e\]/' ~/.bashrc

cat << EOL >> .bashrc
if [ "$color_prompt" = yes ]; then
    if [[ ${EUID} == 0 ]] ; then
        PS1='${debian_chroot:+($debian_chroot)}\[\033[01;31m\]\h\[\033[01;34m\] \W \$\[\033[00m\] '
    else
        PS1='\[\033[00;36m\]\t ${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\] \[\033[01;34m\]\w \[\033[01;33m\]\n\$\[\033[00m\] '
    fi
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h \w \$ '
fi


if [ -f "$HOME/.bash-git-prompt/gitprompt.sh" ]; then
    GIT_PROMPT_ONLY_IN_REPO=1
    source $HOME/.bash-git-prompt/gitprompt.sh
    GIT_PROMPT_THEME=Solarized
fi

alias ll='ls -alFh'
alias t='tree -L '


title(){
    echo -ne "\033]0; $1\t\t\t-\t\t\t${USER}@${HOSTNAME}: ${PWD/$HOME/~}\007"
}
elapsed(){
    /home/dawid/scripts/elapsed.sh $@
}

stty -ixon
EOL

