"highlight syntax
syntax on
"
"enable filetype detection
filetype plugin indent on

"print whitespace chars
set listchars=tab:→\ ,trail:_
set list

"enable mouse
set mouse=a

"use clipboard
set clipboard=unnamed

"set line numbers
set number

"fancy colors
colorscheme desert 

"show currently typed commands
set showcmd

"by default new window will open below or on the right side
set splitbelow
set splitright

"map <F2> as move to nerdtree
nmap <silent> <F2> :execute 'NERDTree'<CR>

"tab settings
set tabstop=4
set softtabstop=4
set shiftwidth=4
set noexpandtab

"new lines without entering insert mode
nnoremap o o<Esc>
nnoremap O O<Esc>

"plugins
execute pathogen#infect()
autocmd vimenter * NERDTree
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
let NERDTreeShowHidden=1
let g:nerdtree_tabs_open_on_console_startup=1
let g:NERDTreeWinSize = 20

let g:airline_powerline_fonts = 1
let g:airline_theme='cool'
let g:airline#extensions#tabline#enabled = 1

"highlight current line
set cursorline
highlight CursorLine ctermbg=23 cterm=bold



