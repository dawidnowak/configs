# remap prefix from 'C-b' to 'C-x'
unbind C-b
set-option -g prefix C-x
bind-key C-x send-prefix

# split panes using s and v
bind s split-window -h
bind v split-window -v
unbind '"'
unbind %

# switch panes using Alt-arrow without prefix
bind -n M-Left select-pane -L
bind -n M-Right select-pane -R
bind -n M-Up select-pane -U
bind -n M-Down select-pane -D

# Enable mouse mode (tmux 2.1 and above)
set -g mouse on

# split 3x2
bind ` split-window -v -p 50 \; select-pane -t 0 \; split-window -h -p 66 \; split-window -h -p 50 \; select-pane -t 3 \; split-window -h -p 66 \; split-window -h -p 50 \;

# colors
set -g default-terminal "screen-256color"
